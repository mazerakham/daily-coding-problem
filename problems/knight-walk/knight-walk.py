def off_board(s):
    return not (0 <= s[0] < 8 and 0 <= s[1] < 8)

def get_neighbors(s):
    return [(s[0]+dx, s[1]+dy) for dx, dy in [(2,1),(1,2),(-1,2),(-2,1),(-2,-1),(-1,-2),(1,-2),(2,-1)]]

def f_memo(s, k):
    if off_board(s):
        return 1

    x = s[0]
    y = s[1]
    if memoized_vals[x][y][k] == None:
        memoized_vals[x][y][k] = 1/8 * sum([f_memo(neighbor, k - 1) for neighbor in get_neighbors(s)])

    return memoized_vals[x][y][k]

def f(s, k):
    '''

    :param s: a pair (i,j) for starting square
    :param k: number of hops
    :return: probability that we've fallen off the board on or before jump k.
    '''
    global memoized_vals
    memoized_vals = [[[None for kk in range(k+1)] for j in range(8)] for i in range(8)]

    for i in range(8):
        for j in range(8):
            for kk in range(k+1):
                memoized_vals[i][j][k] = None

    for i in range(8):
        for j in range(8):
            memoized_vals[i][j][0] = 0

    return f_memo(s, k)



if __name__ == '__main__':
    print(off_board((-1,0))) # True.  Off-board.
    print(off_board((7,7))) # False, on-board.
    print(get_neighbors((2,2))) # ...
    print(f((0,-1), 0)) # 1 since this is off the board.
    print(f((0,0), 1)) # 0.75 since there's only two squares to hop on the board and 6 off.
    print(f((0,0),0)) # 0 since we're still on the board, guaranteed.
    for k in range(40): # These should increase as we get more jumps.
        print(f((3,3), k))
