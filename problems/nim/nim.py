'''
The game of Nim is played as follows. Starting with three heaps, each containing a variable number of items, two players
take turns removing one or more items from a single pile. The player who eventually is forced to take the last stone
loses. For example, if the initial heap sizes are 3, 4, and 5, a game could be played as shown below:

 A  |  B  |  C
-----------------
  3  |  4  |  5
  3  |  1  |  5
  3  |  1  |  3
  0  |  1  |  3
  0  |  1  |  0
  0  |  0  |  0

In other words, to start, the first player takes three items from pile B. The second player responds by removing two
stones from pile C. The game continues in this way until player one takes last stone and loses.
Given a list of non-zero starting values [a, b, c], and assuming optimal play, determine whether the first player has a
forced win.
'''

def generate_tuples_upto(a,b,c):
    for i in range(a+1):
        for j in range(b+1):
            for k in range(c+1):
                yield (i,j,k)

def get_moves(a,b,c):
    moves = []
    for i in range(a):
        moves.append((i,b,c))
    for j in range(b):
        moves.append((a,j,c))
    for k in range(c):
        moves.append((a,b,k))
    return moves

def generate_nim_loss_table(a,b,c):
    loss_table = []

    for (i, j, k) in generate_tuples_upto(a,b,c):
        moves = get_moves(i,j,k)
        if (i,j,k) != (0,0,0) and all(map(lambda move : move not in loss_table, moves)):
            print("Found loss:", (i,j,k))
            loss_table.append((i,j,k))

    return loss_table

def is_nim_win(a, b, c):
    losses = generate_nim_loss_table(a,b,c)
    return (a,b,c) not in losses

def test_nim_win(a, b, c):
    print("Starting setup: ", a, b, c)
    is_win = is_nim_win(a,b,c)
    if is_win:
        print("It's a win for player 1.")
    else:
        print("It's a loss for player 1.")



if __name__ == '__main__':
    print("Printing tuples up to (2, 1, 3):")
    for (i,j,k) in generate_tuples_upto(2,1,3):
        print((i,j,k))

    print("Printing moves from (2, 1, 3):")
    for (i,j,k) in get_moves(2,1,3):
        print(i,j,k)

    test_nim_win(0, 0, 0)
    test_nim_win(1, 0, 0)
    test_nim_win(1, 0, 1)
    test_nim_win(3, 4, 5)