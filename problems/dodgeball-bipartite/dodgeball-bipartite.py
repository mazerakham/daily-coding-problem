def get_bipartite_partition(graph_representation):

    for node, enemies in graph_representation.items():
        # symmetrize.
        graph_representation = dict.copy(graph_representation)
        for enemy in enemies:
            if enemy not in graph_representation:
                graph_representation[enemy] = []
            if node not in graph_representation[enemy]:
                graph_representation[enemy].add(node)

    # All nodes unvisited.  Teams is empty dict that will map nodes to teams.
    teams = dict() # team assignments and a proxy for whether a node has been visited.
    unvisited = set(graph_representation.keys())

    while unvisited:
        node = unvisited.pop()
        if not bfs(unvisited.pop(), graph_representation, teams):
            return None
        for node in teams.keys():
            unvisited.discard(node)

    team1 = set()
    team2 = set()
    for node, team in teams.items():
        if team == 1:
            team1.add(node)
        else:
            team2.add(node)
    return (team1, team2)


def bfs(node, graph_representation, teams):
    queue = []
    queue.append((node, 1))
    while queue:
        node, team = queue.pop(0)
        if not visit(node, team, graph_representation, teams, queue):
            return False
    return True

def visit(node, team, graph_representation, teams, queue):
    # To visit a node with set S.
    #   a. Set this node's set to S.
    #   b. Get all neighbors for the node.
    #   c. For each neighbor:
    #       i. If neighbor is visited, check its team is ^S.  If not, return false immediately.
    #       j. If neighbor is not visited add it to queue.
    #   d. Return true indicating no contradictions encountered here.
    teams[node] = team
    enemies = graph_representation[node]
    for enemy in enemies:
        if enemy in teams:
            if teams[enemy] == team:
                return False
        else:
            queue.append((enemy, 2 if team is 1 else 1))
    return True

def is_bipartite(graph_representation):
    return get_bipartite_partition(graph_representation) is not None

if __name__ == '__main__':
    students = {
        0: [3],
        1: [2],
        2: [1, 4],
        3: [0, 4, 5],
        4: [2, 3],
        5: [3]
    }
    assert (is_bipartite(students) == True)
    print("is_bipartite test 1 passed.")
    partition = get_bipartite_partition(students)
    print("Expect {0,1,4,5} and {2,3} for partition:")
    print(partition)

    students = {
        0: [3],
        1: [2],
        2: [1, 3, 4],
        3: [0, 2, 4, 5],
        4: [2, 3],
        5: [3]
    }
    assert (is_bipartite(students) == False)
    print("is_bipartite test 2 passed by declaring graph non-bipartite.")
