import re

'''
Given a string like
'-1+(8-3)-(-4+5)'
with parentheses, digits, pluses, minuses, parentheses,

Return the result of evaluating the expression.  In this case,
-1 + 5 - 1 = 3
'''


def is_number(expr):
    return True if re.match(r'^-?\d+$', expr) else False

def parse_number(expr):
    if expr[0] == '-':
        return - parse_number(expr[1:])

    return sum(map(lambda p:int(p[1])*10**p[0], enumerate(expr[::-1])))

def has_parens(expr):
    return expr.find('(') != -1

def get_parens(expr):
    count = 0
    for ix, char in enumerate(expr):
        if char == '(':
            if count == 0:
                start = ix
            count += 1

        elif char == ')':
            if count == 1:
                end = ix
                return start, end
            count -= 1

    raise(Exception("Did not find matching parens."))



def split_op(expr):
    '''
    Returns:
        left, op, right, rest
    '''
    result = re.match(r'^(?P<left>-?\d+)(?P<op>\+|-)(?P<right>-?\d+)(?P<rest>.*$)', expr)
    d = result.groupdict()
    return d['left'], d['op'], d['right'], d['rest']

def eval(expr):
    print("Evaluating:", expr)
    if is_number(expr):
        return parse_number(expr)

    elif has_parens(expr):
        start, end = get_parens(expr)
        substitution = str(eval(expr[start+1:end]))
        new_expr = expr[0:start] + substitution + expr[end+1:]
        return eval(new_expr)

    else:
        left, op_char, right, rest = split_op(expr)
        left = parse_number(left)
        right = parse_number(right)
        result = left + right if op_char == '+' else left - right
        return eval(str(result) + rest)

def test_eval(expr):

    print("Expression:", expr)
    result = eval(expr)
    print("Result:", result)

if __name__ == '__main__':
    print("A:", is_number('12340'))   # True
    print("B:", is_number('-12340'))  # True
    print("C:", is_number('1-1'))     # False
    print("D:", parse_number('-12340')) # -12340
    print("E:", parse_number('12340')) # 12340
    print("F:", has_parens('(1234)+5')) # True
    print("G:", has_parens('1234+5')) # False
    print("H:", get_parens('1+(4+5)+3')) # 2, 6
    print("I:", split_op('123+-2')) # ( 123, +, -2, '')
    print("J:", split_op('1+-3+-5+7')) # ( 1, +, -3, +-5+7 )
    test_eval('(1+2)') # 3
    test_eval('-1+(8-3)+(2-3)') # 3
