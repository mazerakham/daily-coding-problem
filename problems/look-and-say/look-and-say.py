def look_and_say(line):
    ret = []
    first = line[0]
    count = 1
    for num in line[1:]:
        if num == first:
            count += 1
        else:
            ret.append(count)
            ret.append(first)
            first = num
            count = 1
    ret.append(count)
    ret.append(first)
    return ret


def print_test_look_and_say(num_lines=10, initial=[1]):
    line = initial
    for i in range(num_lines):
        line = look_and_say(line)
        print(line)

def length_test_look_and_say(trials=1000, initial=[1]):
    line = initial
    ret = []
    for i in range(trials):
        line = look_and_say(line)
        ret.append(len(line))

    return ret

if __name__ == '__main__':
    print_test_look_and_say(num_lines=10, initial=[1])
    print(length_test_look_and_say(trials=60, initial=[1]))