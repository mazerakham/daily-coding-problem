# This repository contains my solutions to my daily coding problems 
(dailycodingproblem.com).

The idea is to practice basic coding consistently, learn patterns, avoid
anti-patterns, improve my debugging skills, and just be a better engineer. 
